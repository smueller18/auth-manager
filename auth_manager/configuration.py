#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

import os
import platform

import distro
import yaml


class Configuration:
    _config: dict = None

    @classmethod
    def load(cls, config_file: str = "application.yml"):
        with open(config_file, "r") as yaml_file:
            cls._config = yaml.safe_load(yaml_file)

        if 'tls_verify' not in cls._config:
            cls._config['tls_verify'] = True

    @classmethod
    def get(cls, item: str = None):
        if cls._config is None:
            cls.load()
        if item is not None:
            return cls._config[item]
        return cls._config


def set_requests_ca_bundle():
    if os.getenv('REQUESTS_CA_BUNDLE') is not None:
        return

    if distro.linux_distribution()[0] == 'Ubuntu':
        os.environ['REQUESTS_CA_BUNDLE'] = '/etc/ssl/certs/ca-certificates.crt'
        return

    if platform.system() == 'Windows':
        pass

    raise NotImplementedError(
        "Setting environment variable REQUESTS_CA_BUNDLE automatically is not supported on your platform."
        "Set REQUESTS_CA_BUNDLE manually to the path of trusted certificates.")

# TODO check why certificate errors appear
# set_requests_ca_bundle()
