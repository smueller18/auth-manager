#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license


class LoginError(Exception):
    pass


class LogoutError(Exception):
    pass


class ParsingError(Exception):
    pass
