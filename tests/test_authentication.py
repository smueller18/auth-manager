#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

import mock
import pytest

from auth_manager.authentication import AuthenticationManager
from auth_manager.configuration import Configuration


class TestAuthenticationManager:

    @staticmethod
    def module_import_mock(driver: str):
        return __import__(f"tests.drivers.{driver.lower()}", fromlist=[driver.lower()])

    def test(self):
        Configuration._config = {
            'instances': [{
                'key': 'key'
            }]
        }
        with pytest.raises(ValueError) as e:
            AuthenticationManager()
        assert str(e.value) == "Missing driver for instance {'key': 'key'}."

        Configuration._config = {
            'instances': [{
                'key': 'key',
                'driver': 'none'
            }]
        }
        with pytest.raises(NotImplementedError) as e:
            AuthenticationManager()
        assert str(e.value) == "Authentication driver 'none' is not implemented."

        Configuration._config = {
            'instances': [{
                'key': 'key',
                'driver': 'Mock',
                'test': 'test',
            }]
        }
        with pytest.raises(TypeError) as e:
            with mock.patch("auth_manager.authentication.AuthenticationManager.module_import", self.module_import_mock):
                AuthenticationManager()
        assert str(e.value.args[0]) == "Wrong parameters for instance 'key'."
        assert e.value.args[1].args[0] == "__init__() got an unexpected keyword argument 'test'"

        Configuration._config = {
            'instances': [{
                'key': 'key',
                'driver': 'Mock',
            }]
        }
        with mock.patch("auth_manager.authentication.AuthenticationManager.module_import", self.module_import_mock):
            auth_manager = AuthenticationManager()
        assert 'key' in auth_manager.get()
