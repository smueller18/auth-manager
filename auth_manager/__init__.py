#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

__author__ = 'Stephan Müller'
__copyright__ = '2018, Stephan Müller'
__license__ = 'MIT'
__version__ = '0.0.1'
