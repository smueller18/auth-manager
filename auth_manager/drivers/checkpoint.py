#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

import json
import logging
from dataclasses import dataclass

import requests
from jsbn import RSAKey

from auth_manager.configuration import Configuration
from auth_manager.exceptions import LoginError, LogoutError
from auth_manager.authentication import AuthenticationInfo, AuthenticationProvider

logger = logging.getLogger(__name__)


@dataclass
class CheckpointInfo(AuthenticationInfo):
    logged_in: bool = False
    user: str = None
    time_to_end_of_session: int = None


class Checkpoint(AuthenticationProvider):

    def __init__(self, key: str, host: str):
        super().__init__(key)
        self._base_url = f'https://{host}'

        self._session = requests.session()
        self._session.verify = Configuration.get('tls_verify')

        self._rsa_key = RSAKey()
        rsa_settings = json.loads(self._session.get(f'{self._base_url}/RSASettings').text)
        self._rsa_key.setPublic(rsa_settings['m'], rsa_settings['e'])

    def info(self) -> CheckpointInfo:
        response = self._session.get(self._base_url + '/GetAttributes')
        status = json.loads(response.text)
        return CheckpointInfo(
            logged_in=status['authenticated'],
            time_to_end_of_session=status['timeToEndOfSession'],
            user=self._session.cookies.get('cpnacportal_username')
        )

    def login(self, username: str, password: str, otp: str) -> CheckpointInfo:

        info = self.info()
        if info.logged_in:
            logger.debug("Already logged in, nothing to do.")
            return info

        encrypted_password = self._rsa_key.encrypt(password + otp)

        # if this is uncommented, SESSION_FAULIRE is returned
        # self._session = requests.session()
        # self._session.verify = Configuration.get('tls_verify')
        # self._session.get(self._base_url + '/GetAttributes')

        response = self._session.post(
            self._base_url + '/Login',
            data={'realm': 'passwordRealm', 'username': username, 'password': encrypted_password},
            # TODO remove?
            # cookies={'cpnacportal_login_type': 'password'}
        )

        if response.status_code != 200:
            raise LoginError(f"Return code != 200 for {self.key}.")

        content = json.loads(response.content)
        if 'type' not in content or content['type'] != 'SUCCESS':
            raise LoginError(f"Unsuccessful login for {self.key}: {content}")

        return self.info()

    def logout(self) -> CheckpointInfo:
        response = self._session.get(
            self._base_url + '/Logoff'
        )
        if response.status_code != 200:
            raise LogoutError(f"Logout for {self.key} failed.")

        return self.info()
