#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

import logging
from dataclasses import dataclass

from auth_manager.authentication import AuthenticationInfo, AuthenticationProvider

logger = logging.getLogger(__name__)


@dataclass
class MockInfo(AuthenticationInfo):
    logged_in: bool = False
    user: str = None


class Mock(AuthenticationProvider):

    def __init__(self, key: str):
        super().__init__(key)

    def info(self) -> MockInfo:
        return MockInfo(False, "test")

    def login(self, username: str, password: str, otp: str) -> MockInfo:
        return MockInfo(True, "test")

    def logout(self) -> MockInfo:
        return MockInfo(False, "test")
