#!/usr/bin/env python3
#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

import argparse
import getpass
import logging
import os

from auth_manager.authentication import AuthenticationManager

LOGGING_LEVEL = os.getenv("LOGGING_LEVEL", "INFO")
logging_format = "%(levelname)8s %(asctime)s %(message)s"
logging.basicConfig(level=logging.getLevelName(LOGGING_LEVEL), format=logging_format)

__dirname__ = os.path.dirname(os.path.abspath(__file__))

parser = argparse.ArgumentParser(prog='auth-manager')
parser.add_argument(
    '-c',
    '--config',
    type=str,
    dest='config_file',
    default="application.yml",
    help='location of application config file'
)
parser.add_argument(
    '-i',
    '--instances',
    type=str,
    dest='instances',
    default=None,
    help='choose instances'
)

subparsers = parser.add_subparsers(help='sub-command help', dest='command')

parser_status = subparsers.add_parser('status', help='status of instances')
parser_login = subparsers.add_parser('login', help='login to instance')
parser_logout = subparsers.add_parser('logout', help='logout instance')


def main(args):
    auth_manager = AuthenticationManager()

    keys = None
    if args.instances is not None:
        keys = args.instances.split(",")

    if args.command == 'status':
        print(os.linesep.join(auth_manager.status(keys)))

    if args.command == 'logout':
        print(os.linesep.join(auth_manager.logout(keys)))

    if args.command == 'login':
        logged_out = AuthenticationManager.get_logged_out(keys)

        if len(logged_out) == 0:
            print("Already logged in.")
            print(os.linesep.join(auth_manager.status()))
            exit(0)
        else:
            print(f"Currently not logged in at: {', '.join(logged_out)}")

        password = getpass.getpass()

        otps = list()
        for key, _ in logged_out.items():
            otps.append(getpass.getpass(f"{key} OTP: "))

        print(os.linesep.join(auth_manager.login(password, otps, instances=logged_out)))


if __name__ == '__main__':
    main(parser.parse_args())
