#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

import logging
from abc import ABCMeta, abstractmethod
from typing import List, Dict

from auth_manager.configuration import Configuration

logger = logging.getLogger(__name__)


class AuthenticationInfo(metaclass=ABCMeta):
    logged_in: bool = False
    user: str = None
    pass


class AuthenticationProvider(metaclass=ABCMeta):
    key: str = None

    def __init__(self, key: str):
        self.key = key

    @abstractmethod
    def info(self) -> AuthenticationInfo:
        raise NotImplementedError

    @abstractmethod
    def login(self, username: str, password: str, otp: str) -> AuthenticationInfo:
        raise NotImplementedError

    @abstractmethod
    def logout(self) -> AuthenticationInfo:
        raise NotImplementedError


class AuthenticationManager:

    def __init__(self):
        self._instances = dict()

        for instance in Configuration.get('instances'):
            if 'driver' not in instance:
                raise ValueError(f"Missing driver for instance {instance}.")

            try:
                drivers = AuthenticationManager.module_import(instance['driver'])
                driver_class = getattr(drivers, instance['driver'])
            except (KeyError, ModuleNotFoundError):
                raise NotImplementedError(f"Authentication driver '{instance['driver']}' is not implemented.")

            params = dict(instance)
            del params['driver']
            try:
                self._instances[instance['key']] = driver_class(**params)
            except TypeError as e:
                raise TypeError(
                    f"Wrong parameters for instance '{instance['key']}'.", e
                )

    @staticmethod
    def module_import(driver: str):
        return __import__(f"auth_manager.drivers.{driver.lower()}", fromlist=[driver.lower()])

    def get(self) -> Dict[str, AuthenticationProvider]:
        return self._instances

    def filter(self, keys) -> Dict[str, AuthenticationProvider]:
        instances = dict()
        for key in keys:
            for instance in self._instances.values():
                if key == instance.key:
                    instances.update({key: instance})
        return instances

    def get_logged_out(self, keys: List[str] = None) -> Dict[str, AuthenticationProvider]:
        instances = dict()
        for key, instance in self.get_instances(keys).items():
            if not instance.info().logged_in:
                instances.update({key: instance})
        return instances

    def status(self, keys: List[str] = None) -> List[str]:
        output = list()
        for key, instance in self.get_instances(keys).items():
            output.append(f'{key}: {instance.info()}')
        return output

    def login(self, password: str, otps: List[str], instances: Dict[str, AuthenticationProvider] = None) -> List[str]:
        output = list()

        if len(otps) < len(instances):
            raise AttributeError("Number of OTPs must be equal with the number of logins")

        otps.reverse()
        for key, instance in instances.items():
            login_info = instance.login(Configuration.get('user'), password, otps.pop())
            output.append(f'{key}: {login_info}')

        return output

    def logout(self, keys: List[str] = None) -> List[str]:
        output = list()
        for key, instance in self.get_instances(keys).items():
            output.append(f'{key}: {instance.logout()}')
        return output

    def get_instances(self, keys: List[str]) -> Dict[str, AuthenticationProvider]:
        if keys is not None:
            return self.filter(keys)

        return self._instances
