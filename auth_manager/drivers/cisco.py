#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

import logging
from dataclasses import dataclass, field
from datetime import datetime

import bs4
import requests

from auth_manager.configuration import Configuration
from auth_manager.exceptions import ParsingError, LoginError, LogoutError
from auth_manager.authentication import AuthenticationInfo, AuthenticationProvider

logger = logging.getLogger(__name__)


@dataclass
class CiscoInfo(AuthenticationInfo):
    logged_in: bool = False
    user: str = None
    credentials_rejected: bool = field(default=False, repr=False)
    session_expired: bool = field(default=False, repr=False)
    ip: str = None
    login_time: datetime = None


class Cisco(AuthenticationProvider):
    _timeout = 10
    _url_login = 'https://{}:9999/netaccess/loginuser.html'
    _url_status = 'https://{}:9999/netaccess/connstatus.html'

    def __init__(self, key: str, host: str):
        self._session = requests.session()
        self._session.verify = Configuration.get('tls_verify')

        super().__init__(key)
        self._url_login = self._url_login.format(host)
        self._url_status = self._url_status.format(host)

    def info(self) -> CiscoInfo:
        response = self._session.get(self._url_status, timeout=self._timeout)
        return self._parse_response(response)

    def login(self, username: str, password: str, otp: str) -> CiscoInfo:

        info = self.info()
        if info.logged_in:
            logger.debug("Already logged in, nothing to do.")
            return info

        # prevent session expired messages for post
        response = self._session.get(self._url_login)
        if response.status_code != 200:
            raise LoginError(f"Page '{response.url} returned 404.")

        response = self._session.post(
            self._url_login,
            data={'username': username, 'password': password + otp, 'sid': 0},
            timeout=self._timeout)
        info = self._parse_response(response)

        if info.credentials_rejected:
            raise PermissionError(f"Credentials rejected for {self.key}, {info}")

        if info.session_expired:
            raise LoginError(f"Session expired for {self.key}, {info}")

        if not info.logged_in:
            raise LoginError(f"Login failed for {self.key}, {info}")

        return info

    def logout(self) -> CiscoInfo:
        info = self._parse_response(requests.post(
            self._url_status,
            data={'logout': 'Log Out Now'},
            timeout=self._timeout)
        )

        if info.logged_in:
            raise LogoutError("Logout failed, still logged in")

        return info

    @staticmethod
    def _parse_response(response: requests.Response) -> CiscoInfo:
        logger.debug(response.text)

        html = bs4.BeautifulSoup(response.text, 'html5lib')
        try:
            info = list(html.find_all('td')[1].children)

            cisco_info = CiscoInfo()

            cisco_info.logged_in = info[0].text == 'You are logged in.'
            textarea = html.find('textarea')
            if textarea is not None:
                cisco_info.credentials_rejected = textarea.text == "Credentials Rejected"
                cisco_info.session_expired = textarea.text == "Session Expired."

            if cisco_info.logged_in:
                cisco_info.user = info[2].split(" ")[1]
                cisco_info.ip = info[4].split(" ")[2]
                cisco_info.login_time = datetime.strptime(
                    info[6].replace(" MEST", "").replace(" MET", ""), 'Since: %H:%M:%S %a %b %d %Y')
            else:
                cisco_info.ip = info[2].split(" ")[2]

            return cisco_info

        except Exception as e:
            raise ParsingError(f"Parsing failed for {response.url}.", e)
