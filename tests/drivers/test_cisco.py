#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

import datetime
import pytest
import requests
import os

from auth_manager.configuration import Configuration
from auth_manager.drivers.cisco import CiscoInfo, Cisco


class TestCisco:

    @pytest.mark.parametrize("content,cisco_info", [
        [
            b"""
                <html>
                <body>
                    <table>
                        <tr>
                            <td><input name=login value="Log In Now"></td>
                            <td><b>You are not logged in.</b><br>User IP: 192.168.209.153<br></td>
                        </tr>
                    </table>
                </body>
                </html>
             """,
            CiscoInfo(
                logged_in=False,
                ip='192.168.209.153',
            )
        ],
        [
            b"""
                <html>
                <body>
                    <form method="post" action="/netaccess/connstatus.html">
                        <p><textarea>Credentials Rejected</textarea></p>
                        <table>
                            <tr>
                                <td><input name=login value="Log In Now"></td>
                                <td><b>You are not logged in.</b><br>User IP: 192.168.209.153<br></td>
                            </tr>
                        </table>
                    </form>
                </body>
                </html>
             """,
            CiscoInfo(
                logged_in=False,
                ip='192.168.209.153',
                credentials_rejected=True
            )
        ],
        [
            b"""
                <html>
                <body>
                    <form method="post" action="/netaccess/connstatus.html">
                        <p><textarea>Session Expired.</textarea></p>
                        <table>
                            <tr>
                                <td><input name=login value="Log In Now"></td>
                                <td><b>You are not logged in.</b><br>User IP: 192.168.209.153<br></td>
                            </tr>
                        </table>
                    </form>
                </body>
                </html>
             """,
            CiscoInfo(
                logged_in=False,
                ip='192.168.209.153',
                session_expired=True
            )
        ],
        [
            b"""
                <html>
                <body>
                    <table>
                        <tr>
                            <td><input name=login value="Log In Now"></td>
                            <td><b>You are logged in.</b><br>User: user<br>User IP: 192.168.209.153<br>Since: 00:00:00 Sat Dec 01 2018</td>
                        </tr>
                    </table>
                </body>
                </html>
             """,
            CiscoInfo(
                logged_in=True,
                ip='192.168.209.153',
                login_time=datetime.datetime(2018, 12, 1, 0, 0, 0),
                user="user"
            )
        ]
    ])
    def test_parse_response(self, content, cisco_info):
        response = requests.Response()
        response._content = content
        response.status_code = 200

        firewall_info = Cisco._parse_response(response)

        assert firewall_info == cisco_info
