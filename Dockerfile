FROM python:3-alpine

WORKDIR /app
COPY requirements.txt /app/

RUN apk add --no-cache --virtual .build-deps gcc musl-dev libffi-dev libressl-dev && \
    pip install --no-cache-dir -r /app/requirements.txt && \
    apk del --no-cache .build-deps

COPY app.py /app/app.py
COPY auth_manager /app/

CMD ["python", "/app/app.py"]
