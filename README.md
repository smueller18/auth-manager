# auth-manager
[![pipeline status](https://gitlab.com/smueller18/auth-manager/badges/master/pipeline.svg)](https://gitlab.com/smueller18/auth-manager/commits/master)
[![documentation](https://smueller18.gitlab.io/auth-manager/documentation.svg)](https://smueller18.gitlab.io/auth-manager/)
[![coverage](https://gitlab.com/smueller18/auth-manager/badges/master/coverage.svg)](https://smueller18.gitlab.io/auth-manager/coverage/)
[![pylint](https://smueller18.gitlab.io/auth-manager/lint/pylint.svg)](https://smueller18.gitlab.io/auth-manager/lint/)
